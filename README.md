HID Task

Instructions:

1 - Open a command line shell change dir to the repo directory.
2 - To build it, run ant on the command line.
    ant  
3 - To run it, type the following in the command line. Note cvs.txt is the input file.
    java -jar build/jar/FooBar.jar cvs.txt
