package fooBar;
import java.io.*;
public class FooBar {
	public static  void main( String args[]) {
		System.out.println("Welcome to FooBar");
		
		if (args.length == 0) {
			System.err.println("Missing input file.");
		}
		else {
			String fileName = args[0];
			int bar=0, foo=0, fooBar=0;
			
			try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
				String line=null;
				while ((line = br.readLine()) != null ) {
					String[] buf = line.split(",");
					if (buf.length < 3){
						System.err.println("Skipping line due to missing data.");
					}
					else { 
						if (buf[1].contentEquals("1"))  // foo
							foo++;
						if (buf[1].contentEquals("1"))  // bar
							bar++;
						if (buf[1].contentEquals("1") && buf[2].contentEquals("1")) //foobar
							fooBar++;
					}
				}
				System.out.println("foo : " + foo);
				System.out.println("bar : " + bar);
				System.out.println("foobar : " + fooBar);
			}
			catch (Exception e)
			{
				System.err.println("An error occured: " + e.getLocalizedMessage());
			}			
		}		
	}	
}
